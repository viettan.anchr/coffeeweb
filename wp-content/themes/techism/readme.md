# Techism WordPress Theme
	By Rajeeb Banstola

## ABOUT Techism

Techism is a light weight, simple yet fully responsive WordPress theme that looks great on any device. Other features include easy customization using theme options page, responsive slider on homepage, optional 1 - 3 columns layout, optional 4 columns footer layout, fast loading, icon fonts , custom menu, header image and background image.

##  FEATURES
Custom Drop-down Menu, Theme Options, Responsive, Suport Live Customizer, Flexible One to Three Column Layout, Custom Widgets, flexible position of sidebar and Support for popular plugins.

## TAGS
Tags Used: light, white, one-column, two-columns,three-columns, right-sidebar, left-sidebar, flexible-width, custom-background, custom-header, custom-menu, editor-style, featured-images, flexible-header, full-width-template, microformats, post-formats, rtl-language-support, sticky-post, theme-options, translation-ready, threaded-comments, custom-menu, full-width-template

## LICENSE

All the theme files, scripts and images are licensed under GNU General Public License Version 2,
see file license.txt

License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Techism WordPress Theme, Copyright (c) 2013 Rajeeb Banstola
Techism is based on the _s (Underscores) starter theme by Automattic Inc. and is distributed under the terms of the GNU General Public License v2 or later.

## INSTALLATION

1. Primary:
 * Login to your wp-admin area and go to Appearance -> Themes.
 * Select Install tab and click on Upload link.
 * Select theme .zip and click on Install now button.
 * If you have any errors, use alternate method.

2. Alternate:
 * Unzip the template file (Techism.zip) that you have downloaded.
 * Via FTP, upload the whole folder (Techism) to your server and place it in the /wp-content/themes/ folder.
 * Do not change directory name.
 * The template files should thus be here now: /wp-content/themes/Techism/index.php (for example).

3. Log into your WP admin panel and click on the Design tab.
4. Now click on the Techism theme to activate it.
5. Complete all of the required inputs on the Techism Options page (in the WP admin panel under Appearance Menu) and click "Save Changes".

## Changelog for techism WordPress Responsive Theme

8/03/2019 - version 3.0.2
* Update sdk

14/03/2017 - version 3.0.1
* Add Support for Custom Logo
* Integrate Freemius

24/02/2016 - version 3.0
* Changed Author and Theme URI

23/08/2015 - version 2.0.9
* Fix for customizer settings not showing up

16/08/2014 - version 2.0.8
* Changed color of menu for better visibility under 768px
* Added settings to hide post meta information

10/05/2014 - version 2.0.7
* Enable excerpt in archive/category/search pages using the previous setting

10/03/2014 - version 2.0.6
* Minor Issue Fixes

08/03/2014 - version 2.0.5
* Added setting to enable/disable site title and tagline above the header image
* Increased slider image width and modified caption css
* Fixed hatom error showing in Google Webmasters Tool
* Fixed the issue of readmore in post page

27/02/2014 - version 2.0.4
* Fixed error with Google Web Fonts setting

27/02/2014 - version 2.0.2
* Fixed the issue with page template ( Full Width Template showing sidebar space blank )
* Improved Folders and Files distribution
* Added Setting to enable full post on homepage
* Added setting to disable Google Web Font - Opensans

22/02/2014 - version 2.0.1
* Minor chnages
* Removed theme options page and added Live customizer for changing settings

22/12/2013 - version 2.0.0
* Header Image Not Stretching problem solved
* Title not showing properly - fixed

17/12/2013 - Version 1.5
* Changed is_home to is_front_page for slider display on homepage

12/12/2013 - Version 1.4
* Changed the Author URI
* Few Other Minor Fixes

11/24/2013 - Version 1.0.3
* Added default thumbnail in slider when no featured image is available

11/20/2013 - Version 1.0.2
* Fixed a Issue for Left Sidebar in Responsive
* Added Link to Theme Homepage in Footer
* Added License, Readme and Changelog in txt
* Fixed the issue with Older Posts and Newer Posts section
* Added License Information in Readme.txt file

11/14/2013 - Version 1.0.1
* Minor Style Changes

11/12/2013 - Version 1.0.0
* Initial Public Release










